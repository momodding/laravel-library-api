FROM php:7.2-fpm

ADD database.sql /docker-entrypoint-initdb.d

ENV composer_hash 544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061

COPY composer.lock composer.json /var/www/

COPY database /var/www/database

WORKDIR /var/www

RUN apt-get update && apt-get -y install git && apt-get -y install zip

# RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
# php -r "if (hash_file('SHA384', 'composer-setup.php') === '${composer_hash}') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
# php composer-setup.php --install-dir=/usr/bin --filename=composer && \
# php -r "unlink('composer-setup.php');" \
# && php composer.phar install --no-dev --no-scripts \
# && rm composer.phar
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# RUN groupadd -g 1000 www
# RUN useradd -u 1000 -ms /bin/bash -g www www

COPY . /var/www

# COPY --chown=www:www . /var/www

RUN chown -R www-data:www-data \
    /var/www/storage \
    /var/www/bootstrap/cache

RUN  apt-get install -y libmcrypt-dev \
    libmagickwand-dev --no-install-recommends \
    && pecl install mcrypt-1.0.2 \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-enable mcrypt

# RUN mv .env.prod .env
RUN composer install

RUN php artisan key:generate \
    && php artisan optimize:clear